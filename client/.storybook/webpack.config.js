const path = require('path');

module.exports = ({ config }) => {

	// use installed babel-loader which is v8.0-beta (which is meant to work with @babel/core@7)
	config.module.rules[0].use[0].loader = require.resolve('babel-loader')

	config.module.rules.push({
		test: /\.s[ac]ss$/i,
		use: [
			'style-loader',
			'css-loader',
			{
				loader: 'postcss-loader',
				options: {
					plugins: [
						require('autoprefixer')({}),
					]
				}
			},
			'sass-loader',

		],
		include: path.resolve(__dirname, '../')
	})

	// use @babel/preset-react for JSX and env (instead of staged presets)
	config.module.rules[0].use[0].options.presets = [
		require.resolve('@babel/preset-react'),
		require.resolve('@babel/preset-env')
	]


	// Prefer Gatsby ES6 entrypoint (module) over commonjs (main) entrypoint
	config.resolve.mainFields = ['browser', 'module', 'main']

	return config
}
