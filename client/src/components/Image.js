import React, {useEffect, useState} from 'react'
import PropTypes from 'prop-types'

const Image = ({ src, alt, ...rest }) => {

    const [imageSrc, setSrc] = useState('data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7')

    useEffect(()=>{
        setSrc(src)
    },[])

    const onLoad = () => {

    }

    const onError = () => {

    }

    return <img onLoad={onLoad} onError={onError} src={imageSrc} {...rest} alt={alt} style={{width: '100%'}} />
}

Image.propTypes = {
    src: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired
}

export default Image
