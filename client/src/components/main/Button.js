import React from "react";
import styled from "styled-components";

const Style = styled.button`
  border: none;
  padding: 0;
  background: transparent;
  margin: 0 10px 10px 0;
  line-height: 1;
  cursor: pointer;
  & > div {
  border: 1px solid #817970;
  border-radius: 7px;
  font-family: 'Syne Mono', monospace;
  text-transform: Capitalize;
  letter-spacing: -0.01rem;
  padding: 8px 12px;
  background-color: #fff;
  font-size: 1rem;
    &:hover {
      color: #fff;
      border-color: transparent;
      background: ${props => (props.shouldHover ? "" : "#423931")};
    }
  }
`

export default function Button({...rest}) {
    return (
        <Style {...rest}>
            <div>{rest.children}</div>
        </Style>
    )
}
