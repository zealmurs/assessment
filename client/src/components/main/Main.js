import React, {useState} from "react"
import cx from "classnames"
import {gql, useQuery} from '@apollo/client'
import Modal from '../modal'
import Button from './Button'
import Joke from "../joke/Joke";
import Title from "./title.svg";
import styled from "styled-components";

const Container = styled.div`
  padding: 2rem;
  & > div {
    width: 100%;
    max-width: 600px;
    margin: 0 auto;
    text-align: center;
  }
`

const H3 = styled.h3`
  font-size: 21px;
  font-family: 'Syne Mono', monospace;
`

const H5 = styled.h3`
  font-size: 17px;
  font-family: 'Syne Mono', monospace;
  margin-bottom: 36px;
  position: relative;
  top: -12px;
`

function Main() {

    const [modal, setModal] = useState({
        toggleModal: null,
        showModal: false,
        category: null
    })

    const viewJoke = () => {
        setModal({
            showModal: !modal.showModal
        })
    }

    const GET_CATEGORIES = gql`
            query {
                    categories {
                        name
                    }
                }`

    const {loading, error, data} = useQuery(GET_CATEGORIES)

    if (loading){
        return 'Loading...'
    }

    if (error) {
        return `Error! ${error.message}`
    }

    return (
        <Container>

            <div>
                <Modal toggleModal={viewJoke} showModal={modal.showModal}>
                    { modal.category && <Joke category={modal.category} /> }
                </Modal>

                <div>
                    <img src={'/chuck.png'} alt="Chuck" style={{maxWidth: '300px'}}/>
                    <H3>Just one</H3>
                    <img src={Title} alt="Chuck Joke" style={{maxWidth: '450px'}}/>
                    <H5>might change your day, choose wisely!</H5>
                </div>

                {
                    data.categories.map((category, i) => {
                        return(
                            <Button
                                key={i}
                                title={category.name}
                                className={cx({active: category.name === modal.category })}
                                onClick={()=> setModal({
                                    showModal: !modal.showModal,
                                    category: category.name
                                })}
                            >
                                {category.name}
                            </Button>
                        )
                    })
                }
            </div>

        </Container>
    )
}

export default Main
