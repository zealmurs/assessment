import React, { Fragment, PureComponent } from 'react'
import { createPortal } from 'react-dom'
import PropTypes from 'prop-types'
import { ReactComponent as Close } from './close.svg'
import styled from "styled-components";

const Closer = styled.button`
	padding: 0;
	border: none;
	background: transparent;
	position: absolute;
	right: 1.6rem;
	top: 1.6rem;
	cursor: pointer;
	& > svg{
		width: 30px;
	}
`;

const Overlay = styled.div`
width: 100vw;
height: 100vh;
position: fixed;
top: 0;
left: 0;
z-index: 2;
background: rgba(0,0,0, 0.6);
`;

const Dom = styled.div`
width: 100vw;
height: 100vh;
position: fixed;
top: 0;
left: 0;
z-index: 3;
& > div{
	width: 100vw;
    height: 100vh;
    margin: 0 auto;
    overflow-y: auto;
    overflow-x: hidden;  
    background: #eee9e5;
    display: flex;
    align-items: center;
    justify-content: center;
    & > svg{
    	position: relative;
    	left: 12.5px;
    }
}
`;

class ModalWrapper extends PureComponent {

	closeModal = ({ target }) => {
		if (this.modal && !this.modal.contains(target)) {
			//this.props.toggleModal()
		}
	}

	componentDidMount() {
		document.addEventListener('click', this.closeModal, false)
	}

	componentWillUnmount() {
		document.removeEventListener('click', this.closeModal, false)
	}

	render = () =>
		createPortal(
			<Fragment>
				<Overlay />
				<Dom>
					<div ref={(node) => (this.modal = node)} className="dom-child">
						{this.props.children}
					</div>
				</Dom>
			</Fragment>,
			document.body
		)
}

const Modal = ({ children, toggleModal, showModal }) => {
	return (
		<>
			{showModal && (
				<ModalWrapper toggleModal={toggleModal}>
					<Closer onClick={toggleModal}>
						<Close />
					</Closer>
					{children}
				</ModalWrapper>
			)}
		</>
	)
}

ModalWrapper.propTypes = {
	children: PropTypes.node.isRequired,
	toggleModal: PropTypes.func.isRequired
}

Modal.propTypes = {
	children: PropTypes.any,
	toggleModal: PropTypes.func.isRequired,
	showModal: PropTypes.bool.isRequired
}

export default Modal
