import React from 'react'
import ModalEl from '../src/Modal'

export default {
	title: 'Modal'
}

class Story extends React.Component {
	constructor(props) {
		super(props)
		this.open = this.open.bind(this)
		this.state = {
			toggleModal: null,
			showModal: false
		}
	}

	open() {
		this.setState({
			showModal: true
		})
	}

	render() {
		return (
			<div>
				<ModalEl toggleModal={this.open} showModal={this.state.showModal} />
				Hello <button onClick={this.open}>Toggle</button>
			</div>
		)
	}
}

export const Modal = () => <Story />
