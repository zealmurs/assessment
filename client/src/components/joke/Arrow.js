import React from "react";
import { ReactComponent as ArrowIcon } from './src/arrow.svg'
import styled from "styled-components";
import Spinner from "../spinner/Spinner";

const ArrowContainer = styled.button`
    background: transparent;
    border: none;
    margin: 0 auto;
    min-width: 100px;
    display: block;
    outline: none;
    min-height: 50px;
    & > div{
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        & > .text{
            font-family: 'Syne Mono', monospace;
            margin-right: 10px;
        }
        & > svg{
            width: 55px
        }
    }
`;

function Arrow({...rest}) {
    return(
        <ArrowContainer {...rest}>
            <div>
                {
                    rest.isLoading
                        ?
                        <Spinner />
                        :
                        <>
                            <div className="text">
                                Next
                            </div>
                            <ArrowIcon />
                        </>
                }

            </div>
        </ArrowContainer>
    )
}

export default Arrow
