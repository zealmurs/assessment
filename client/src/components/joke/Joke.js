import React, {useState} from "react"
import PropTypes from 'prop-types'
import {gql, useQuery} from '@apollo/client'
import cx from "classnames";
import styled from "styled-components";
import {ReactComponent as Quotes} from './src/quote.svg'
import Arrow from "./Arrow";
import Spinner from "../spinner/Spinner";

const Category = styled.div`
    opacity: 0.5;
    font-size: 70%;
    padding-top: 8px;
    text-transform: capitalize;
`
const Value = styled.div`
    font-family: 'Syne Mono', monospace;
    font-size: 1.2rem;
    letter-spacing: -0.02rem;
    text-align: center;
    padding 3rem 3rem 1rem;
    max-width: 350px;
    line-height: 1.4rem;
    &.loading{
        opacity: 0.3;
    }
`

function Joke({category}) {

    const GET_CATEGORIES = gql`
            query { 
                joke(category: "${category}"){ 
                    id 
                    value 
                    icon_url 
                    url 
                } 
            }
`

    const {loading, error, data, refetch} = useQuery(GET_CATEGORIES)
    const [isReLoading, setIsReLoading] = useState(false)

    if (loading) {
        return <Spinner/>
    }

    if (error) {
        return `Error! ${error.message}`
    }

    return !data && !loading ? <>No Joke</> : (
        <div>
            <div style={{textAlign: 'center', position: 'relative'}}>
                <img src={'/chuck-laughing.png'} alt="Chuck" style={{maxWidth: '250px', margin: '0 auto'}}/>
                <Quotes style={{
                    width: '80px',
                    margin: '0 auto',
                    position: 'absolute',
                    bottom: '-20px',
                    left: 'calc(50% - 40px)'
                }}/>
            </div>

            <Value className={cx({loading: isReLoading})}>
                {data.joke.value}&quot;
                <br />
                <Category>&mdash;&mdash; {category}</Category>
            </Value>

            <Arrow isLoading={isReLoading} onClick={() => {
                setIsReLoading(true)
                try {
                    refetch.call().then(()=> setIsReLoading(false))
                } catch (e) {

                }
            }}/>
        </div>
    )
}

Joke.propTypes = {
    category: PropTypes.string.isRequired
}

export default Joke

