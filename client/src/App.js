import React from "react";
import { ApolloProvider} from '@apollo/client';
import client from './config/api'
import Main from "./components/main/Main";

const App = () => (
    <ApolloProvider client={client}>
        <Main />
    </ApolloProvider>
)

export default App
